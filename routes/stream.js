const express = require('express');
const router = express.Router();

const fs = require('fs');

const User = require('../models/user');
const Movie = require('../models/movie');

const isAuthenticated = require('../middleware/auth/isAuthenticated');

const objectRepository = { User };

router.use(isAuthenticated(objectRepository));

router.get('/:id', async (req, res, next) => {
	try {
		const { id } = req.params;

		if (typeof id === 'undefined') {
			throw new Error('No id specified');
		}

		const movie = await Movie.findOne({ where: { id, downloaded: true } });
		if (movie === null) {
			throw new Error('No such movie');
		}

		const path = movie.moviePath;
		const stat = fs.statSync(path);
		const fileSize = stat.size;

		const range = req.headers.range;
		if (range) {
			const parts = range.replace(/bytes=/, '').split('-');
			const start = parseInt(parts[0], 10);
			const end = parts[1] ? parseInt(parts[1], 10) : fileSize - 1;

			const chunksize = end - start + 1;
			const file = fs.createReadStream(path, { start, end });
			const head = {
				'Content-Range': `bytes ${start}-${end}/${fileSize}`,
				'Accept-Ranges': 'bytes',
				'Content-Length': chunksize,
				'Content-Type': 'video/mp4'
			};

			res.writeHead(206, head);
			file.pipe(res);
		} else {
			const head = {
				'Content-Length': fileSize,
				'Content-Type': 'video/mp4'
			};
			res.writeHead(200, head);
			fs.createReadStream(path).pipe(res);
		}
	} catch (err) {
		return next(err);
	}
});

module.exports = router;
