const express = require('express');
const router = express.Router();

const User = require('../models/user');

const login = require('../middleware/auth/login');
const setToken = require('../middleware/auth/setToken');

const objectRepository = { User };

router.post('/login', login(objectRepository), setToken());

module.exports = router;
