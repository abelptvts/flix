const express = require('express');
const send = require('send');
const router = express.Router();
const multer = require('multer');

const upload = multer({
	dest: process.env.SUBTITLE_UPLOAD_PATH || 'subtitles/'
});

const { db } = require('../database');
const Movie = require('../models/movie');
const User = require('../models/user');
const Subtitle = require('../models/subtitle');

const fetchLatestMovies = require('../middleware/yts/fetchLatestMovies');
const fetchBestMovies = require('../middleware/yts/fetchBestMovies');
const addTorrents = require('../middleware/torrent/addTorrents');

const isAuthenticated = require('../middleware/auth/isAuthenticated');
const getMovies = require('../middleware/movies/getMovies');
const getMovie = require('../middleware/movies/getMovie');
const srtToVtt = require('../middleware/subtitles/srtToVtt');
const addSubtitle = require('../middleware/subtitles/addSubtitle');
const getSubtitle = require('../middleware/subtitles/getSubtitle');
const removeSubtitle = require('../middleware/subtitles/removeSubtitle');

const { fixDownloaded } = require('../torrent');

const objectRepository = { db, Movie, User, Subtitle };

// internal, called by crontab
router.get(
	'/internal/movies',
	fetchLatestMovies(),
	fetchBestMovies(),
	addTorrents(),
	(req, res) => {
		res.json({ status: 'ok' });
	}
);

router.get('/internal/fix', async () => { await fixDownloaded() })

router.use(isAuthenticated(objectRepository));

// api
router.get('/movies', getMovies(objectRepository), (req, res) => {
	res.json({
		data: res.locals.movies
	});
});

router.get('/movie/:id', getMovie(objectRepository), (req, res) => {
	res.json({
		data: res.locals.movie
	});
});

router.get('/images/:filename', (req, res, next) => {
	const coverImagesPath =
		process.env.COVER_IMAGES_PATH || '/home/abelputovits/DEV/flix/coverImages/';

	send(req, coverImagesPath + req.params.filename)
		.on('error', error => next(error))
		.pipe(res);
});

// subtitles upload
router.post(
	'/subtitles',
	upload.single('subtitle'),
	srtToVtt(),
	addSubtitle(objectRepository),
	(req, res) => {
		console.log(req.file);
		res.json({ status: 'ok' });
	}
);

router.get('/subtitles/:id', getSubtitle(objectRepository), (req, res) => {
	send(req, res.locals.subtitle.path)
		.on('error', error => next(error))
		.pipe(res);
});

router.get('/subtitles/remove/:id', removeSubtitle(objectRepository), (req, res) => {
	res.json({ status: 'ok' });
});

module.exports = router;
