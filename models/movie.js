const Sequelize = require('sequelize');
const { db } = require('../database');

const Movie = db.define('movie', {
	title: { type: Sequelize.STRING, allowNull: false },
	description: { type: Sequelize.TEXT, allowNull: true },
	transmissionId: { type: Sequelize.INTEGER, allowNull: true },
	moviePath: { type: Sequelize.STRING, allowNull: true },
	rating: { type: Sequelize.FLOAT, allowNull: false },
	year: { type: Sequelize.INTEGER, allowNull: false },
	downloaded: { type: Sequelize.BOOLEAN, allowNull: false },
	coverImage: { type: Sequelize.STRING, allowNull: false }
});

module.exports = Movie;
