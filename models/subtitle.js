const Sequelize = require('sequelize');
const { db } = require('../database');
const Movie = require('./movie');

const Subtitle = db.define('subtitle', {
	language: { type: Sequelize.STRING, allowNull: false },
	path: { type: Sequelize.STRING, allowNull: false }
});

Subtitle.belongsTo(Movie);
Movie.hasMany(Subtitle);

module.exports = Subtitle;
