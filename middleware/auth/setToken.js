const jwt = require('jsonwebtoken');

function sign(object) {
	const secret = process.env.JWT_SECRET || 'asdasdasasdasasdasd';
	return new Promise((resolve, reject) => {
		jwt.sign(object, secret, (err, signed) => {
			if (err) {
				return reject(err);
			}

			return resolve(signed);
		});
	});
}

function setToken(objectRepository) {
	return async (req, res, next) => {
		try {
			const { user } = res.locals;
			const token = await sign({ id: user.id });
			return res.json({ token });
		} catch (err) {
			return next(err);
		}
	};
}

module.exports = setToken;
