const bcrpyt = require('bcrypt');

function verifyPassword(password, hash) {
	return new Promise((resolve, reject) => {
		bcrpyt.compare(password, hash, (err, isValid) => {
			if (err) {
				return reject(err);
			}

			return resolve(isValid);
		});
	});
}

function login(objectRepository) {
	const { User } = objectRepository;
	return async (req, res, next) => {
		try {
			const { email, password } = req.body;

			if (typeof email === 'undefined') {
				throw new Error('email is required');
			}

			if (typeof password === 'undefined') {
				throw new Error('password is required');
			}

			const user = await User.findOne({ where: { email } });

			if (user === null) {
				const err = new Error('Not authenticated!');
				err.status = 401;
				throw err;
			}

			const isPasswordValid = await verifyPassword(password, user.password);
			if (!isPasswordValid) {
				const err = new Error('Not authenticated!');
				err.status = 401;
				throw err;
			}

			res.locals = { user };
			return next();
		} catch (err) {
			return next(err);
		}
	};
}

module.exports = login;
