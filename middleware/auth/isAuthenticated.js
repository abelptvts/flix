const jwt = require('jsonwebtoken');

function verify(token) {
	const secret = process.env.JWT_SECRET || 'asdasdasasdasasdasd';
	return new Promise((resolve, reject) => {
		jwt.verify(token, secret, (err, result) => {
			if (err) {
				return reject(err);
			}

			return resolve(result);
		});
	});
}

function isAuthenticated(objectRepository) {
	const { User } = objectRepository;

	return async (req, res, next) => {
		try {
			let token = req.headers.authorization;

			if (typeof token === 'undefined') {
				token = req.query.token;

				if (typeof token === 'undefined') {
					const err = new Error('Not authenticated!');
					err.status = 401;
					throw err;
				}
			}

			const { id } = await verify(token);
			const user = await User.findOne({ where: { id } });

			if (user === null) {
				const err = new Error('Not authenticated!');
				err.status = 401;
				throw err;
			}

			req.user = { id };
			return next();
		} catch (err) {
			return next(err);
		}
	};
}

module.exports = isAuthenticated;
