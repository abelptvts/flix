const { add } = require('../../torrent');

function addTorrents(objectRepository) {
	return async (req, res, next) => {
		try {
			const { bestMovies, latestMovies } = res.locals;

			await add(bestMovies);
			console.log('added best');
			await add(latestMovies);
			console.log('added latest');

			return next();
		} catch (err) {
			return next(err);
		}
	};
}

module.exports = addTorrents;
