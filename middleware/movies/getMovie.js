function getMovie(objectRepository) {
	const { Movie, Subtitle } = objectRepository;
	return async (req, res, next) => {
		try {
			const { id } = req.params;
			const options = {};

			if (typeof id === 'undefined') {
				throw new Error('Query param id is neccessary');
			}

			options.where = { id };
			options.include = [Subtitle];

			const movie = await Movie.findOne(options);
			res.locals = { movie };

			return next();
		} catch (err) {
			return next(err);
		}
	};
}

module.exports = getMovie;
