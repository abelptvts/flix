function getMovies(objectRepository) {
	const { db, Movie } = objectRepository;
	return async (req, res, next) => {
		try {
			const { limit, offset, downloaded, keyword } = req.query;

			if (typeof limit === 'undefined' || typeof offset == 'undefined') {
				throw new Error('limit and offset required');
			}

			if (typeof downloaded === 'undefined') {
				throw new Error('Query param downloaded is neccessary');
			}

			const movies = await db.query(
				`select * from movies where downloaded = ${downloaded} ${
					typeof keyword !== 'undefined' ? `and title like '%${keyword}%' ` : ''
				} order by createdAt desc limit ${offset}, ${limit}`,
				{ type: db.QueryTypes.SELECT }
			);

			res.locals.movies = movies.map(movie => ({
				id: movie.id,
				title: movie.title,
				rating: movie.rating,
				year: movie.year,
				coverImage: movie.coverImage,
				downloaded: movie.downloaded
			}));

			return next();
		} catch (err) {
			return next(err);
		}
	};
}

module.exports = getMovies;
