const fetch = require('node-fetch');
const fs = require('fs');

const url =
	'https://yts.am/api/v2/list_movies.json?minimum_rating=6&sort_by=date_added&limit=5&page=';

const paginationFileName = './yts_current_pagination_latest.json';
let page = 1;

function fetchLatestMovies() {
	return async (req, res, next) => {
		try {
			if (fs.existsSync(paginationFileName)) {
				const pageJson = JSON.parse(fs.readFileSync(paginationFileName));
				page = pageJson.page;
			}

			const result = await fetch(url + page);
			const resultJson = await result.json();
			if (resultJson.status !== 'ok' || typeof resultJson.data === 'undefined') {
				console.log(url + page);
				console.log(resultJson);
				return next(new Error('YTS fetch failed!'));
			}
			// update pagination
			fs.writeFileSync(paginationFileName, JSON.stringify({ page: page + 1 }));

			res.locals.latestMovies = resultJson.data.movies;
			return next();
		} catch (err) {
			return next(err);
		}
	};
}

module.exports = fetchLatestMovies;
