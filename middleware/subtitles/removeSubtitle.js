const fs = require('fs');

function removeSubtitle(objectRepository) {
	const { Subtitle } = objectRepository;

	return async (req, res, next) => {
		try {
			const { id } = req.params;

			const subtitle = await Subtitle.findOne({ where: { id } });

			if (subtitle === null) {
				throw new Error('no such subtitle');
			}

			fs.unlink(subtitle.path, err => {
				if (err) {
					console.log(err);
				}

				console.log('subtitle deleted');
			});

			await Subtitle.destroy({ where: { id } });
			return next();
		} catch (err) {
			return next(err);
		}
	};
}

module.exports = removeSubtitle;
