function addSubtitle(objectRepository) {
	const { Subtitle, Movie } = objectRepository;
	return async (req, res, next) => {
		try {
			const { id, language } = req.body;

			if (typeof id === 'undefined') {
				throw new Error('field id required');
			}

			if (typeof language === 'undefined') {
				throw new Error('field language required');
			}

			const movie = await Movie.findOne({ where: { id } });
			if (movie === null) {
				throw new Error('no such movie');
			}

			await Subtitle.create({ language, movieId: id, path: res.locals.path });
			return next();
		} catch (err) {
			return next(err);
		}
	};
}

module.exports = addSubtitle;
