const fs = require('fs');
const convertSrt = require('srt-to-vtt');
const path = require('path');

const subtitlesPath = process.env.SUBTITLE_UPLOAD_PATH || '/home/abelputovits/dev/flix/subtitles/';

function srtToVtt() {
	return (req, res, next) => {
		const { originalname, filename } = req.file;

		if (path.extname(originalname) !== '.srt') {
			return next(new Error('only .srt allowed'));
		}

		const vttFilePath = subtitlesPath + filename + '.vtt';

		fs.createReadStream(subtitlesPath + filename)
			.pipe(convertSrt())
			.pipe(fs.createWriteStream(vttFilePath));

		// remove srt version
		fs.unlink(subtitlesPath + filename, err => {
			if (err) {
				console.log(err);
			}

			console.log('Delete srt version of subtitle');
		});

		res.locals.path = vttFilePath;
		return next();
	};
}

module.exports = srtToVtt;
