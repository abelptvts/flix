function getSubtitle(objectRepository) {
	const { Subtitle } = objectRepository;
	return async (req, res, next) => {
		try {
			const { id } = req.params;

			const subtitle = await Subtitle.findOne({ where: { id } });
			if (subtitle === null) {
				throw new Error('no such subtitle');
			}

			res.locals.subtitle = subtitle;
			return next();
		} catch (err) {
			return next(err);
		}
	};
}

module.exports = getSubtitle;
