const path = require('path');
const fs = require('fs');
const fetch = require('node-fetch');

const transmission = require('./transmission');
const Movie = require('../models/movie');
const { db } = require('../database');

function getMovieFilePath(movie) {
	const downloadPath =
		process.env.TRANSMISSION_DOWNLOAD_PATH || '/var/lib/transmission-daemon/downloads/';

	let movieFileName = '';
	movie.info.files.forEach(file => {
		if (path.extname(file.name) === '.mp4') {
			movieFileName = file.name;
		}
	});

	return downloadPath + movieFileName;
}

function ls(path) {
	return new Promise((resolve, reject) => {
		fs.readdir(path, (err, items) => {
			if(err) {
				return reject(err);
			}

			return resolve(items);
		})
	})
}

function waitFor(ids) {
	ids.forEach(id =>
		transmission.waitForComplete(id).then(async movie => {
			try {
				console.log(`updating ${movie.title} in db...`);
				await db.query(
					`update movies set moviePath = '${getMovieFilePath(
						movie
					)}', downloaded = 1, transmissionId = null where transmissionId = ${movie.id}`,
					{ raw: true }
				);
			} catch (err) {
				console.log(err);
			}
		})
	);
}

function fetchCoverImage(movie) {
	const coverImagesPath =
		process.env.COVER_IMAGES_PATH || '/home/abelputovits/DEV/flix/coverImages/';
	return new Promise(async (resolve, reject) => {
		try {
			const result = await fetch(movie.medium_cover_image);
			const fileName = movie.title + '.jpg';
			const stream = fs.createWriteStream(coverImagesPath + fileName);
			result.body.pipe(stream);
			return resolve(fileName);
		} catch (err) {
			return reject(err);
		}
	});
}

function getTorrentUrl(movie) {
	let url = movie.torrents[0].url;
	movie.torrents.forEach(torrent => {
		if (torrent.quality === '1080p') {
			url = torrent.url;
		}
	});

	return url;
}

async function add(movies) {
    const ids = [];
    for (const movie of movies) {
        try {
            const existing = await Movie.findOne({ where: { title: movie.title } });
            if (existing !== null) {
            	console.log(`movie: ${movie.title} already exists!`);
                continue;
            }

            let now = new Date();
            console.log('fetching cover image...');
            const coverFileName = await fetchCoverImage(movie);
			console.log(`fetched cover image in: ...s`);

            const id = await transmission.download(getTorrentUrl(movie));
            console.log('download started');
            // insert into db
            await Movie.create({
                title: movie.title,
                description: movie.description_full,
                transmissionId: id,
                rating: movie.rating,
                year: movie.year,
                downloaded: false,
                coverImage: '/images/' + encodeURI(coverFileName)
            });

            console.log('inserted into db');

            ids.push(id);
		} catch (err) {
        	console.log(err);
		}
    }

    console.log('Waiting for movies to download');

    waitFor(ids);
}

/**
 * Fix movies that have already been downloaded but not added to DB
 * */

async function fixDownloaded() {
	try {
        const url = 'https://yts.am/api/v2/list_movies.json;';
        const downloadPath =
            process.env.TRANSMISSION_DOWNLOAD_PATH || '/var/lib/transmission-daemon/downloads/';

		const contents = await ls(downloadPath);
		for(const content of contents) {
            let isDownloaded = true;
            const files = await ls(`${downloadPath}${content}/`);
            let moviePath = null;

            for(const file of files) {
            	if(file.endsWith('.part')) {
            		isDownloaded = false;
            		break;
				}

				if(file.endsWith('.mp4')) {
					moviePath = `${downloadPath}${content}/${file}`;
				}
			}

            let title = content;
            if(title.includes('[')) {
                title = title.split('[')[0]
            }

			if(!isDownloaded) {
				console.log(`${title} is not yet downloaded!`);
				continue;
			}

			const result = await fetch(`${url}?query_term=${title}`);
            const resultJson = await result.json();
            const { movies } = resultJson.data;
            if(!movies) {
                console.log(`${title} has 0 hits!`);
                continue;
            }
            if(movies.length > 1) {
                console.log(`${title} has many hits`);
                continue;
			}

			const movie = movies[0];
            const existing = await Movie.findOne({ where: { title: movie.title } });
            if (existing) {
                if(existing.moviePath === null) {

                    await Movie.update(
                        {
                            moviePath
                        },
                        {
                            where: { title: movie.title }
                        }
                    )
                }

                continue;
            }
            
            await Movie.create({
                title: movie.title,
                description: movie.description_full,
                transmissionId: null,
                rating: movie.rating,
                year: movie.year,
                downloaded: true,
                coverImage: '/images/' + encodeURI(`${movie.title}.jpg`)
            });
        }
	} catch (err) {
		console.log(err);
	} finally {
		console.log('END');
    }
}

async function checkOngoingTorrents() {
	try {
		// wait for ongoing torrents
		const moviesInProgress = await Movie.findAll({ where: { downloaded: false } });
		console.log(`movies in progress: ${moviesInProgress.map(movie => movie.title)}`);
		waitFor(moviesInProgress.map(movie => movie.transmissionId));
	} catch (err) {
		console.log(err);
	}
}

module.exports = { checkOngoingTorrents, add, fixDownloaded };
