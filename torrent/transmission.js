const Transmission = require('transmission');

const transmission = new Transmission({
	username: 'transmission',
	password: 'transmission'
});

function waitForComplete(id) {
	return new Promise((resolve, reject) => {
		transmission.waitForState(id, 'SEED', (err, result) => {
			if (err) {
				return reject(err);
			}

			console.log('Downloaded: ', id);
			transmission.remove(id, err => {
				if (err) {
					return reject(err);
				}
			});
			return resolve({ id, info: result });
		});
	});
}

function download(url) {
	return new Promise((resolve, reject) => {
		transmission.addUrl(url, (err, result) => {
			if (err) {
				return reject(err);
			}

			const { id } = result;
			transmission.waitForState(id, 'DOWNLOAD', (err, result) => {
				if (err) {
					return reject(err);
				}

				console.log('Added new torrent with id: ', id);
				return resolve(id);
			});
		});
	});
}

module.exports = {
	download,
	waitForComplete
};
