const Sequelize = require('sequelize');
const config = require('./config');

const db = new Sequelize(config.database, config.username, config.password, {
	host: config.host,
	logging: false,
	dialect: config.dialect
});

module.exports = {
	db,
	testConnection: () => db.authenticate().then(() => console.log('Connected to DB!'))
};
