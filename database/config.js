module.exports = {
	database: process.env.DB_NAME || 'flix',
	username: process.env.DB_USER || 'flix',
	password: process.env.DB_PASS || '',
	host: process.env.DB_HOST || 'localhost',
	dialect: process.env.DB_DIALECT || 'mysql'
};
